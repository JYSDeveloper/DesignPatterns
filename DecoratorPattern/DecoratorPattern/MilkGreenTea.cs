﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DecoratorPattern
{
    class MilkGreenTea : Product
    {
        public int price { get ; set; }
        public String name { get; set; }
        public MilkGreenTea()
        {
            //初始化价格
            price = 15;
            name = "奶绿";
        }
        public void make()
        {
            Console.WriteLine(("你的产品为：" + name);
        }
    }
}
