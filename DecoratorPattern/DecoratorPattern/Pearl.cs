﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DecoratorPattern
{
     class Pearl : Additives
    {
        public Pearl(Product product) : base(product)
        {
            this.name = this.product.name + "+" + "珍珠";
            this.price = this.product.price + 5;
        }
    }
}
