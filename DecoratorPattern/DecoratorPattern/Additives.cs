﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DecoratorPattern
{
    //装饰类
    public abstract class Additives : Product
    {
        public int price { get ; set ; }
        public String name { get; set; }
        //被装饰的物品
        protected Product product;
        public Additives(Product product)
        {
            this.product = product;
        }


        public void make()
        {
            Console.WriteLine("你的产品为：" + this.name);
            Console.WriteLine("你的价格：" + this.price);
        }
    }
}
