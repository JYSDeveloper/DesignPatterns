﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DecoratorPattern
{
    class MilkRedTea:Product
    {
        public int price { get; set; }
        public String name { get; set; }
        public MilkRedTea()
        {
            //初始化价格
            price = 12;
            name = "奶茶";
        }
        public void make()
        {
            Console.WriteLine("你的产品为："+this.name);
        }
    }
}
