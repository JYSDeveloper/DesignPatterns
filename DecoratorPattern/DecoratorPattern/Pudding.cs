﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DecoratorPattern
{
    class Pudding : Additives
    {
        public Pudding(Product product) : base(product)
        {
            this.name = this.product.name + "+布丁" ;
            this.price = this.product.price + 6;
        }
    }
}
