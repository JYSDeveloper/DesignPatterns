﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ObserverPattern
{
    class Product2 : Observer
    {
        public Product2(Material material)
        {
            this.material = material;
            this.material.attach(this);
        }
        public override void update()
        {
            //计算商品价格
            int price = 18 + this.material.getPrice();
            Console.WriteLine("商品2价格更新为："+price);
        }
    }
}
