﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ObserverPattern
{
    abstract class Observer
    {
        protected Material material;
        public abstract void update();
    }
}
