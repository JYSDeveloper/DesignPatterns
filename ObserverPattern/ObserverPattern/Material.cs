﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ObserverPattern
{
    class Material
    {
        //所有商店的集合
        private List<Observer> shops = new List<Observer>();
        private int price { get; set; }
        //返回原材料的价格
        public int getPrice()
        {
            return price;
        }
        //设置原材料的价格并更新商店的价格
        public void setPrice(int price)
        {
            this.price = price;
            this.updateAllPrice();
        }
        //增添观察者也就是商店
        public void attach(Observer observer)
        {
            shops.Add(observer);
        }
        //更新价格
        public void updateAllPrice()
        {
            foreach (Observer observer in shops)
            {
                observer.update();
            }
        }
    }
}
