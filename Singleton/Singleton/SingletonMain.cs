﻿using System;

namespace Singleton
{
    class SingletonMain
    {
        static void Main(string[] args)
        {
            //获得实例
            TestSingleton test1 = TestSingleton.getInstance();
            //设置姓名
            test1.setName("Jack");
            //获得实例
            TestSingleton test2 = TestSingleton.getInstance();
            test2.setName("Jane");

            //输出名字
            test1.printInfo();
            test2.printInfo();

            //判断是否为同一对象
            if(test1 == test2)
            {
                Console.WriteLine("为同一个实例");
            } else
            {
                Console.WriteLine("Hello World!");
            }            
        }
    }
}
