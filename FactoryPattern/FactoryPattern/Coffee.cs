﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryPattern
{
    class Coffee : Drinks
    {
        public string name { get; set; }
        public Coffee()
        {
            name = "coffee";
        }
        public void make()
        {
            Console.WriteLine("This is "+name);
        }
    }
}
