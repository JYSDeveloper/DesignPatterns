﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryPattern
{
    class MilkTea :Drinks
    {
        public string name { get; set; }
        public MilkTea()
        {
            name = "milk tea";
        }
        public void make()
        {
            Console.WriteLine("This is " + name);
        }
    }
}
