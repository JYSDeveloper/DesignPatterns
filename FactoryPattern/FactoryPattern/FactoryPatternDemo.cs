﻿using System;

namespace FactoryPattern
{
    class FactoryPatternDemo
    {
        static void Main(string[] args)
        {
            DrinksFactory drinkShop = new DrinksFactory();

            //获取咖啡对象
            Drinks coffee = drinkShop.getDrinks("COFFEE");
            coffee.make();

            //获取奶茶对象
            Drinks milkTea = drinkShop.getDrinks("MILKTEA");
            milkTea.make();
           
        }
    }
}
