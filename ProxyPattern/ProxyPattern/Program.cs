﻿using System;
using Castle.Core;
using Castle.DynamicProxy;

namespace ProxyPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("外卖中介帮你买奶茶");
            Shop proxyShop = CreateShopProxy();
            Console.WriteLine("中介成功购买奶茶");
            proxyShop.BuyDrinks();
        }
        private static Shop CreateShopProxy()
        {
            ProxyGenerator proxyGenerator = new ProxyGenerator();
            Shop proxyShop = proxyGenerator.CreateClassProxy<Shop>(new ProxyShop());
            return proxyShop;
        }
    }
}
