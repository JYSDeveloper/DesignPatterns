﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProxyPattern
{
    public interface Shop
    {
        //购买饮料
        void BuyDrinks();
    }
}
