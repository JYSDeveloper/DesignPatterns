﻿using System;
using System.Collections.Generic;
using System.Text;
using Castle.DynamicProxy;

namespace ProxyPattern
{
    //建立一个拦截器实现shop接口
    public class ProxyShop : IInterceptor
    {
        public void Intercept(IInvocation invocation)
        {
            //执行奶茶店的逻辑
            invocation.Proceed();
            CollectIntermediaryFees();
        }

        private void CollectIntermediaryFees()
        {
            Console.WriteLine("中介收取配送费！");
        }
    }
}
